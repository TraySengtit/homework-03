import React from 'react'
import { View, Text, StyleSheet, ImageBackground, StatusBar, TouchableHighlight, Image, ScrollView, Pressable, Alert } from 'react-native'
import FlatCard from './src/component/FlatCard'
import FlatCard2 from './src/component/FlatCard2';

const App = () => {

  const myListAlert = () =>
    Alert.alert(
      "My List",
      "Do you agree ?",
      [
        { text: "Cancel" },
        { text: "OK" }
      ]
    );
  const playAlert = () =>
    Alert.alert(
      "Play",
      "Do you agree ?",
      [
        { text: "Cancel" },
        { text: "OK" }
      ]
    );
  const infoAlert = () =>
    Alert.alert(
      "Info",
      "Do you agree ?",
      [
        { text: "Cancel" },
        { text: "OK" }
      ]
    );
  return (
    <View style={styles.container}>
    <ScrollView>
      <StatusBar translucent backgroundColor='transparent'/>
      <ImageBackground source={{uri: 'https://i.mydramalist.com/vAnBe_4f.jpg'}} style={styles.imgTop}>
      <Image
      style={{ height: 50, width: 50, position: 'absolute', top: 10, left: 10 }}
      source={{
        uri: 'https://cdn140.picsart.com/331140999123211.png?type=webp&to=min&r=640',
      }}
    />
      <View style={{ marginTop: 70, flexDirection: 'row', justifyContent: 'space-around' }}>
                <Text style={{ color: 'white' }}>TV Show</Text>
                <Text style={{ color: 'white' }}>Movie Show</Text>
                <Text style={{ color: 'white' }}>Category</Text>
              </View>
      </ImageBackground>
      <View style={styles.contentStyle}>
        <View style={styles.playSide}>
          <View style={{flexGrow: 1, backgroundColor: '#181818', alignItems: 'center'}}>
              <Pressable onPress={myListAlert}>
                <Image
                  style={{ height: 30, width: 30, top: 5 , color: 'white', borderRadius: 50}}
                  source={
                    require('./src/assets/images/add.png')
                  }
                />
                <Text style={{color: 'white', marginLeft: -6}}>My List</Text>
              </Pressable>
          </View>
          <View style={{flexGrow: 1, backgroundColor: '#F9F9F9', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRadius: 5}}>
              <Pressable onPress={myListAlert}>
                <Image
                  style={{ height: 30, width: 30, top: 5 , color: 'white', borderRadius: 50}}
                  source={
                    require('./src/assets/images/play1.png')
                  }
                />
                <Text  style={{ marginLeft: 3}}>Play</Text>
              </Pressable>
          </View>
          <View style={{flexGrow: 1, backgroundColor: '#181818', justifyContent: 'center', alignItems: 'center'}}>
              <Pressable onPress={myListAlert}>
                <Image
                  style={{ height: 32, width: 32, top: 5 , color: 'white', borderRadius: 50}}
                  source={
                    require('./src/assets/images/info.png')
                  }
                />
                <Text style={{color: 'white', marginLeft: 6}}>info</Text>
              </Pressable>
          </View>
        </View>
        <View>
          <Text style={{fontSize: 27, fontWeight: "bold", color: 'white'}}>Popular on Netflix</Text>
        </View>
        <View style={{height: 230}}>
          <FlatCard/>
        </View>
        <View>
          <Text style={{fontSize: 27, fontWeight: "bold", color: 'white'}}>Trending Now</Text>
        </View>
        <View style={{height: 300}}>
          <FlatCard2/>
        </View>
      </View>
      </ScrollView>
      <View style={styles.footerStyle}>
        <View style={{ alignItems: 'center' }}>
          <Image
            style={{ height: 20, width: 25 }}
            source={
              require('./src/assets/images/home.png')
            }
          />
          <Text style={{color: 'white'}}>Home</Text>
        </View>
        <View style={{ alignItems: 'center' }}>
          <Image
            style={{ height: 30, width: 30 , borderRadius: 50}}
            source={
              require('./src/assets/images/shorts.png')
            }
          />
          <Text style={{color: 'white'}}>Shorts</Text>
        </View>
        <View style={{ alignItems: 'center' }}>
          <Image
            style={{ height: 35, width: 35 , borderRadius: 50}}
            source={
              require('./src/assets/images/m.png')
            }
          />
        </View>
        <View style={{ alignItems: 'center' }}>
          <Image
            style={{ height: 30, width: 30}}
            source={
              require('./src/assets/images/subb.png')
            }
          />
          <Text style={{color: 'white'}}>Subscribe</Text>
        </View>
        <View style={{ alignItems: 'center' }}>
          <Image
            style={{ height: 30, width: 30 }}
            source={
              require('./src/assets/images/library.png')
            }
          />
          <Text style={{color: 'white'}}>Library</Text>
        </View>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  imgTop: {
    width: '100%',
    height : 350,
    flex: 1,
  },
  contentStyle: {
    flex: 1,
    backgroundColor: '#181818',
    width: '100%',
    height: '100%',
  },
  playSide: {
    // flex: 1,
    width: '100%',
    height: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  footerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 50,
    backgroundColor: '#515151',
    alignItems: 'center'
  },
  // listStyle: {

  // },
})
export default App
