import React from 'react';
import { View, FlatList, StyleSheet, Text, ImageBackground, Image } from 'react-native';

const DATA = [
  {
    id: '1',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSXZzNobyDnQJnadjxEAXOO6_HHIBNNJSClZ-ZhCuWjLfXMjLlRySRkTQjHWpZSXfAcgTU&usqp=CAU',
  },
  {
    id: '2',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT4xTepwA1yR8dAOV6-cjydIGsgkvsyhqSat2Jfpas1aEW2Xzy0JR47KAETFwcg0nqNXAc&usqp=CAU',
  },
  {
    id: '3',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQbiE89JyrsyGG3XvaxZE-m3_s7ts5nuIMtKUPgyB0TqDvO7LjL0ZEA5meHJFOvYTy7KFE&usqp=CAU',
  },
  {
    id: '4',
    img: 'https://s.yimg.com/ny/api/res/1.2/hjS_V7_ce5tNfM4vflrBNQ--/YXBwaWQ9aGlnaGxhbmRlcjt3PTk2MA--/https://s.yimg.com/os/creatr-uploaded-images/2021-03/78fe3d80-886e-11eb-9f56-33be7681b123',
  }
];

const renderItem = ({ item }) => (
  <View style={styles.item}>
      <ImageBackground
        style={{ height: '100%', width: '100%' }}
        source={{
          uri: item.img,
        }}
      />
    <Image
      style={{ height: 40, width: 40, position: 'absolute' }}
      source={{
        uri: 'https://cdn140.picsart.com/331140999123211.png?type=webp&to=min&r=640',
      }}
    />
  </View>
);


const FlatCard2 = () => {

  return (
    <View style={styles.container}>
      <FlatList
        scrollEnabled={true}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'purple',
    marginVertical: 8,
    height: 280,
    width: '100%',
  },
});

export default FlatCard2;